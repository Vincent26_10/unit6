package unit6;

public class PrintPatternsMain {
	public static final String FORMAT_NUM = "%4d";
	public static final String FORMAT_STRING = "%4s";
	public static final String FORMAT_NUM2 = "%3s     ";

	public static void main(String[] args) {
		// printPatternA(7);
		// System.out.println();
		// printPatternB(7);
		// System.out.println();
		// printPatternC(7);
		// System.out.println();
		// printPatternD(7);
		// System.out.println();
		// printPatternE(7);
		// System.out.println();
		// printPatternF(7);
		// System.out.println();
		// printPatternG(7);
		// System.out.println();
		// printPatternH(7);
		// System.out.println();
		// printPatternI(7);
		// System.out.println();
		// printPatternJ(6);
		// System.out.println();
		// printPatternK(6);
		// System.out.println();
		// printPatternL(6);
		// System.out.println();
		// printPatternM(8);
		// System.out.println();
		// printPatternN(8);
		// System.out.println();
		// printPatternO(8);
		// System.out.println();
		// printPatternP(8);
		// printPatternQ(8);
		// printPatternR(8);
		// printPatternS(8);
		// printPatternT(8);
		// printPatternU(8);
		// printATriangle(8);
		// printBTriangle(8);
		printCTriangle(8);
	}

	public static void printBTriangle(int numRows) {
		int[][] arr;

		arr = new int[numRows][numRows];
		arr[0][0] = 1;
		arr[1][0] = 1;
		arr[1][1] = 1;
		System.out.println(String.format(FORMAT_NUM, arr[0][0]));
		System.out.print(String.format(FORMAT_NUM, arr[1][0]));
		System.out.println(String.format(FORMAT_NUM, arr[1][1]));
		for (int i = 2; i < numRows; i++) {
			for (int j = 0; j <= i; j++) {
				if (j == 0 || j == i) {
					arr[i][j] = 1;
				} else {
					arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
				}
				System.out.print(String.format(FORMAT_NUM, arr[i][j]));
			}
			System.out.println();
		}
	}

	public static void printCTriangle(int numRows) {
		int[][] arr;

		arr = new int[numRows][numRows];
		arr[0][0] = 1;
		arr[1][0] = 1;
		arr[1][1] = 1;
		for (int i = numRows - 1; i >= 1; i--) {
			System.out.print(String.format(FORMAT_STRING, "  "));
		}
		System.out.println(String.format(FORMAT_NUM2, arr[0][0]));
		for (int i = numRows - 2; i >= 1; i--) {
			System.out.print(String.format(FORMAT_STRING, "  "));
		}
		System.out.print(String.format(FORMAT_NUM2, arr[1][0]));
		System.out.println(String.format(FORMAT_NUM2, arr[1][1]));

		for (int i = 2; i < numRows; i++) {
			boolean first = true;

			for (int j = 0; j <= i; j++) {
				if (j == 0 || j == i) {
					arr[i][j] = 1;
				} else {
					arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
				}
				if (first) {
					for (int k = numRows - (i + 1); k > 0; k--) {
						System.out.print(String.format(FORMAT_STRING, "  "));
						first = false;

					}
				}
				System.out.print(String.format(FORMAT_NUM2, arr[i][j]));
			}
			System.out.println();
		}
	}

	public static void printATriangle(int numRows) {
		int var;
		for (int i = 1; i <= numRows; i++) {

			for (int j = numRows - i; j > 0; j--) {
				System.out.print(String.format(FORMAT_STRING, "  "));
			}
			var = 1;
			for (int j = 0; j < i; j++) {

				System.out.print(String.format(FORMAT_NUM, var));
				var = var * 2;
			}
			var = var / 2;
			var = var / 2;
			for (int j = i - 1; j > 0; j--) {

				System.out.print(String.format(FORMAT_NUM, var));
				var = var / 2;
			}

			System.out.println();
		}

	}

	public static void printPatternA(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.print("\n");
		}
	}

	public static void printPatternB(int size) {
		for (int i = size; i >= 1; i--) {
			for (int j = i; j >= 1; j--) {
				System.out.print("# ");
			}
			System.out.print("\n");
		}
	}

	public static void printPatternC(int size) {
		int count = 0;
		for (int i = size; i >= 1; i--) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
			count++;
		}
	}

	public static void printPatternD(int size) {
		int count = size - 1;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
			count--;
		}
	}

	public static void printPatternE(int size) {
		for (int row = 1; row <= size; row++) {
			for (int col = 1; col <= size; col++) {
				if (row == 1 || row == size || col == 1 || col == size) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternF(int size) {
		for (int row = 1; row <= size; row++) {
			for (int col = 1; col <= size; col++) {
				if (row == 1 || row == size || col == row) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternG(int size) {
		for (int row = 1; row <= size; row++) {
			for (int col = 1; col <= size; col++) {
				if (row == 1 || row == size || row + col == size + 1) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternH(int size) {
		for (int row = 1; row <= size; row++) {
			for (int col = 1; col <= size; col++) {
				if (row == 1 || row == size || row + col == size + 1 || col == row) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternI(int size) {
		for (int row = 1; row <= size; row++) {
			for (int col = 1; col <= size; col++) {
				if (row == 1 || row == size || row + col == size + 1 || col == row || col == 1 || col == size) {
					System.out.print("# ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternJ(int size) {
		int count = 0;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print("# ");
			}
			for (int j = size - (count + 1); j >= 1; j--) {
				System.out.print("# ");
			}
			count++;
			System.out.println();
		}
	}

	public static void printPatternK(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = size - i; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = 0; j < i - 1; j++) {
				System.out.print("# ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
		}
	}

	public static void printPatternL(int size) {
		int count = (size / 2) - 1;
		for (int i = 1; i <= (size / 2); i++) {
			System.out.print("  ");
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			for (int j = 1; j <= (size / 2) - count; j++) {
				System.out.print("# ");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print("# ");
			}
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			count--;
			System.out.println();
		}
		printPatternJ((size / 2) + 1);

	}

	public static void printPatternM(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();

		}
	}

	public static void printPatternN(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j < i; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - i + 1; j++) {
				System.out.print(j + " ");
			}
			System.out.println();

		}
	}

	public static void printPatternO(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = size - i; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = 8; j > size - i; j--) {
				System.out.print(j + " ");
			}
			System.out.println();

		}
	}

	public static void printPatternP(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = size - i + 1; j > 0; j--) {
				System.out.print(j + " ");
			}

			System.out.println();

		}
	}

	public static void printPatternQ(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = size - i; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = 1; j < i; j++) {
				System.out.print(j + " ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}

	public static void printPatternR(int size) {
		int count = 0;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print(j + " ");
			}
			for (int j = size - (count + 1); j >= 1; j--) {
				System.out.print(j + " ");
			}
			count++;
			System.out.println();
		}
	}

	public static void printPatternS(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			for (int j = size - i; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = size - i - 1; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = i; j > 0; j--) {
				if (j != size) {
					System.out.print(j + " ");
				}
			}
			System.out.println();

		}
	}

	public static void printPatternT(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size - i + 1; j++) {
				System.out.print(j + " ");
			}
			for (int j = i - 1; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = i - 2; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = size - i + 1; j > 0; j--) {
				if (j != size) {
					System.out.print(j + " ");
				}
			}
			System.out.println();

		}
	}

	public static void printPatternU(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = size - i; j > 0; j--) {
				System.out.print("  ");
			}
			for (int j = i; j < i * 2; j++) {
				if (j >= 10) {
					System.out.print((j - 10) + " ");
				} else {
					System.out.print(j + " ");
				}
			}
			for (int j = ((i * 2) - 2); j >= i; j--) {
				if (j >= 10) {
					System.out.print((j - 10) + " ");
				} else {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}

}
